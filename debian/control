Source: afterburner.fx
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: tony mancill <tmancill@debian.org>
Build-Depends: debhelper (>= 11),
 default-jdk,
 maven-debian-helper (>= 2.1)
Build-Depends-Indep: default-jdk-doc,
 junit4,
 libatinject-jsr330-api-java,
 libatinject-jsr330-api-java-doc,
 libfindbugs-annotations-java,
 libgeronimo-annotation-1.3-spec-java,
 libmaven-javadoc-plugin-java,
 libmockito-java,
 libopenjfx-java,
 xauth,
 xvfb
Standards-Version: 4.4.0
Vcs-Git: https://salsa.debian.org/java-team/afterburner.fx.git
Vcs-Browser: https://salsa.debian.org/java-team/afterburner.fx
Homepage: http://afterburner.adam-bien.com

Package: libafterburner.fx-java
Architecture: all
Depends: ${misc:Depends}, ${maven:Depends}
Suggests: ${maven:OptionalDepends}, libafterburner.fx-java-doc
Description: minimalistic JavaFX MVP framework
 afterburner.fx is a minimalistic JavaFX MVP framework based on Convention
 over Configuration and Dependency Injection, providing the following:
 .
  - "Zero-Configuration" javax.inject.Inject DI of models or services
    into presenters.
  - Convention-based unification of presenter, view, FXML and css.
  - Conventional resource bundle loading.
  - Injection of System.getProperties.
  - Injection of presenter-local configuration properties (system
    properties are overriding the local configuration).
 .
 Afterburner is a "Just-Enough-Framework" extracted from
 airhacks-control and used in airpad, lightfish and floyd applications.

Package: libafterburner.fx-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: ${maven:DocDepends}, ${maven:DocOptionalDepends}
Suggests: libafterburner.fx-java
Description: Documentation for afterburner.fx
 afterburner.fx is a minimalistic JavaFX MVP framework based on Convention
 over Configuration and Dependency Injection
 .
 This package contains the API documentation of libafterburner.fx-java.
